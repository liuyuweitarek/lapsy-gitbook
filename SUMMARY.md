# Summary

* [Introduction](README.md)
* [Getting Started](guide/usage.md)
* [Design Script](script/designscript.md)
	- [Example](script/example.md)
	- [Test Script](script/testscript.md)
* [Add Chit-chat Model](model/chitchat.md)
* [Integrate Robot Interface](interface/socialrobot.md)




