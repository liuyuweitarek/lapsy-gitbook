# Introduction

Welcome to LaPsy!

This is the Distributed Psychological Evaluation Dialogue System, which embeds psychological questionnaires into daily conversation based on sentence vectors of user utterances and offers APIs interface of Android Robotics

Demo:

<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0"width="788.54" height="443" type="text/html" src="https://www.youtube.com/embed/fjalezI212I?autoplay=0&fs=1&iv_load_policy=3&showinfo=1&rel=0&cc_load_policy=0&start=0&end=0&origin=http://youtubeembedcode.com"><div><small><a href="https://youtubeembedcode.com/de/">youtubeembedcode de</a></small></div><div><small><a href="https://bettingsidorutansvensklicens.nu/">betting utan svensk licens</a></small></div></iframe>

---

* [Getting Started](guide/usage.md)
* [Psycho Script Path](script/designscript.md)
	- [Example](script/example.md)
	- [Test Script](script/testscript.md)
* [Chit-chat Path](model/chitchat.md)
* [Robot Interface](interface/socialrobot.md)

---


